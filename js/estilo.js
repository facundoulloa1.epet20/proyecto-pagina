document.querySelector('.menu-btn').addEventListener('click', () => {
    document.querySelector('.nav-menu').classList.toggle('show');
});

ScrollReveal().reveal('.inicio');
ScrollReveal().reveal('.new-cards', { delay: 400 });
ScrollReveal().reveal('.cards-baner-one', { delay: 400 });
ScrollReveal().reveal('.cards-baner-two', { delay: 400 });